﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1Termin02.Primer2
{
    class ArrayClass2
    {
        static void Main(string[] args)
        {
            int[] A = { 1, 2, 3, 4, 5 };    //deklaracija, alokacija i inicijalizacija

            //prikazi sumu niza A
            int suma = 0;
            for (int i = 0; i < A.Length; i++)
            {
                suma += A[i]; // suma = suma + niz[i];
            }

            Console.WriteLine("Suma niza A je:" + suma + "\n");

            // Pronaci maksimalni element niza 10, 4, 6, 11, 15, 2
            // Primer sa foreach petljom

            int[] B = { 10, 4, 6, 11, 15, 2 };
            int max = B[0];
            foreach (int br in B)
            {
                if (br > max)
                {
                    max = br;
                }
            }

            Console.WriteLine("Maksimalan element niza B je:" + max + "\n");

            Console.ReadKey();
        }
    }
}
