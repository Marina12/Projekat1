﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1Termin02.Primer6
{
    class ArrayListClass1
    {
        static void Main(string[] args)
        {
            ArrayList list = new ArrayList();
            list.Add("Hello");
            list.Add("!");
            list.Add("World");
            list.Add("!");
            list.RemoveAt(1);
            //uklanja prvi element koji se podudara
            list.Remove("!");

            Console.WriteLine("Prva vrednost je: " + list[0]);
            Console.WriteLine("Da li sadrži ! : " + list.Contains("!"));

            //object tip se mora cast-ovati na određen tip
            string s = (string)list[1];

            Console.WriteLine("My list:");
            Console.WriteLine("\tCount: {0}", list.Count);
            Console.WriteLine("\tCapacity: {0}", list.Capacity);
            Console.Write("\tValues:");
            PrintValues(list);

            Console.ReadKey();
        }

        public static void PrintValues(ArrayList myList)
        {
            foreach (Object obj in myList)
            {
                Console.Write(" {0}", obj);
            }
            Console.WriteLine();
        }
    }
}
