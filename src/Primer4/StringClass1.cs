﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1Termin02.Primer4
{
    class StringClass1
    {
        public static void Main(String[] args)
        {

            //Definisanje promenljive tipa string	
            string s1 = "Hello ";
            string s2 = s1;
            s1 += "World";

            System.Console.WriteLine(s2); //Ispis: Hello

            String s3 = "Ovo je";
            String s4 = "je string";
            Console.WriteLine(s3.Substring(2)); // o je
            Console.WriteLine(s4[3]); // s
            Console.WriteLine(s3.Equals(s4)); //False
            Console.WriteLine(s3.IndexOf("je")); // 4 , ako nema podstringa vratice -1
            Console.WriteLine(s4.Length); //9
            Console.WriteLine(s3.Trim()); //Ovo je , inace skida whitespaces sa pocetka i kraja
            Console.WriteLine(s4.StartsWith("je")); //True
            Console.WriteLine(s4.Replace("je","jeste")); //jeste string
            Console.WriteLine(String.Concat("Ovo ",s4)); //Ovo je string

            //formatiranje stringova metodom String.Format
            Decimal cena = 17.36721m;
            string s = String.Format("Trenutna cena je {0,5:C2} po osobi.", cena);
            Console.WriteLine(s);

            Int32 osobe = 3;
            s = String.Format("Ukupna cena je {0:C2} za {1} osobe.",new object[] { cena*osobe, osobe });
            Console.WriteLine(s);

            Console.WriteLine(String.Format("Decimalna tačka: \t{0:0.0} \t{0:0.000}",cena));

            //interpolacija stringova
            Console.WriteLine("\n\nInterpolacija stringova");
            s = $"Trenutna cena je {cena,5:C2} po osobi.";
            Console.WriteLine(s);

            string interStr = $"Ukupna cena je {cena*osobe,0:C2} za {osobe} osobe.";
            Console.WriteLine(interStr);
            interStr = $"Decimalna tačka:{cena,-10:0.0}{cena,10:0.000}";
            Console.WriteLine(interStr);

            Console.ReadKey();
        }
    }
}
