﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1Termin02.Primer4
{
    public class StringClass2
    {
        public static void Main(String[] args)
        {

            //Definisanje promenljive tipa string	
            String a = "ovo je tekst";
            String a1 = "OvO JE TeKsT";
            String a2 = String.Concat(a, ' ', a1);
            string a3 = new string('\n',4);
            string[] words = { "Hello", "and", "welcome", "to", ".NET", "world!" , a3};
            Console.WriteLine(String.Join(" ", words));

            //Provera da li su stringovi sadrze isti niz karaktera
            //1. način
            //if (a == a1) 
            //2. način
            if (a.Equals(a1))
            {
                Console.WriteLine("Nizovi a i a1 su slicni");
            }
            else
            {
                Console.WriteLine("Nizovi a i a1 nisu slicni");
            }

            if (String.Equals(a, a1, StringComparison.OrdinalIgnoreCase))
            {
                Console.WriteLine("Nizovi a i a1 su slicni zanemarivsi velika i mala slova");
            }

            //prebacivanje velikih u mala slova
            Console.WriteLine("OvO JE TeKsT".ToLower());

            //podstring
            Console.WriteLine("ovo je tekst".Substring(0, 5));

            //Provera da li string a zapoceinje sa karakterima iz stringa a1
            if ("ovo je tekst".StartsWith("ovo je"))
            {
                Console.WriteLine("Zapocinje");
            }

            //Provera da li string a sadrzi rec
            if ("ovo je tekst".Contains("tekst"))
            {
                Console.WriteLine("Sadrzi rec");
            }


            //leksikografsko poredjenje stringova
            String name = "Aco";
            String name1 = "Ana";
            if (name.CompareTo(name1) > 0)      //razlika pozicija c i n 
                Console.WriteLine("posle");
            else if (name.CompareTo(name1) < 0)
                Console.WriteLine("pre");
            else
                Console.WriteLine("isti");

            //String je Immutable Objects
            //Immutable Objects je objekat kome se definiše vrednost u trenutku njegovog kreiranja

            //greska - nije moguce manjati 
            //name[0]='R';

            //da li su stringovi različiti objekti ?
            string str1 = "Sunce izlazi i zalazi";
            string str2 = "Sunce izlazi i zalazi";

            //ReferenceEquals - poredi adrese stringova
            Console.WriteLine("Adrese strigova str1 i str2 " + (String.ReferenceEquals(str1, str2) ? "su iste" : "nisu iste"));
            str2 = "Sunce izlazi i zalazi" + "!";
            Console.WriteLine("Adrese strigova str1 i str2 " + (String.ReferenceEquals(str1 + "!", str2) ? "su iste" : "nisu iste"));

            if (String.ReferenceEquals(str1, str2))
                Console.WriteLine("str1 i str2 su isti objekti");
            else
                Console.WriteLine("str1 i str2 nisu isti objekti");

            //String.Copy - kopira string u zasebni objekat
            string str3 = String.Copy(str1);

            if (String.ReferenceEquals(str1, str3))
                Console.WriteLine("str1 i str2 su isti objekti");
            else
                Console.WriteLine("str1 i str2 nisu isti objekti");


            Console.ReadKey();
        }
    }
}
